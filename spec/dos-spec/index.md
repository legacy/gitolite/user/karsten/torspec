# Denial-of-service prevention mechanisms in Tor

This document covers the strategy, motivation, and implementation for denial-of-service mitigation systems designed into Tor.

The older `dos-spec` document is now the [Memory exhaustion](./memory-exhaustion.md) section here.

An in-depth description of the proof of work mechanism for onion services, originally [proposal 327](../../proposals/327-pow-over-intro.txt), is now in the [Proof of Work for onion service introduction](../hspow-spec/index.md) spec.