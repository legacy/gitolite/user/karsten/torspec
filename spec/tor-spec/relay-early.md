<a id="tor-spec.txt-5.6"></a>

# Handling RELAY_EARLY cells{#handling-relay-early-cells}

A RELAY_EARLY cell is designed to limit the length any circuit can reach.
When an OR receives a RELAY_EARLY cell, and the next node in the circuit
is speaking v2 of the link protocol or later, the OR relays the cell as a
RELAY_EARLY cell.  Otherwise, older Tors will relay it as a RELAY cell.

If a node ever receives more than 8 RELAY_EARLY cells on a given
outbound circuit, it SHOULD close the circuit. If it receives any
inbound RELAY_EARLY cells, it MUST close the circuit immediately.

When speaking v2 of the link protocol or later, clients MUST only send
EXTEND/EXTEND2 message inside RELAY_EARLY cells.  Clients SHOULD send the first
~8 relay cells that are not targeted at the first hop of any circuit as
RELAY_EARLY cells too, in order to partially conceal the circuit length.

\[Starting with Tor 0.2.3.11-alpha, relays should reject any
EXTEND/EXTEND2 cell not received in a RELAY_EARLY cell.\]
